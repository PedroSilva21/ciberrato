package com.minderaschool.explorer.agent;

import com.minderaschool.explorer.client.Robot;

public class Brain {
    private static final double TIME_TO_ROTATE = 38.0;

    private String robName;
    private Robot robot;

    private State currentState = State.WAIT;

    enum State {WAIT, TURN_LEFT, TURN_RIGHT}

    ;

    private double startTurnTime;
    private double startwaitTime;


    // Constructor
    public Brain(String robName, int pos, String host) {
        this.robName = robName;
        robot = new Robot();

        // register robot in simulator
        robot.initRobot(robName, pos, host);


    }

    /**
     * reads a new message, decides what to do and sends action to simulator
     */
    public void mainLoop() {
        robot.readSensors();

        double time = robot.getTime();
        System.out.println(time + ": Waiting to start");
        while (time <= 0.0001 && time >= -0.0001) {
            // do nothing, just wait until it starts
            robot.readSensors();
            time = robot.getTime();

            System.out.print(".");
            System.out.flush();
        }

        System.out.println(time + ": Started");
        while (true) {
            robot.readSensors();
            decide();
        }
    }

    /**
     * basic reactive decision algorithm, decides action based on current sensor values
     */
    public void decide() {
        double currentTime = robot.getTime();
        System.out.println(currentTime + ": " + currentState);

        switch (currentState) {
            case WAIT:
                System.out.println("robot.driveMotors(0.0, 0.0);");
                robot.driveMotors(0.0, 0.0);

                if (startwaitTime < 0) {
                    startwaitTime = currentTime;
                }

                if (currentTime - startwaitTime > TIME_TO_ROTATE) {
                    System.out.println("Changing state to TURN_LEFT");
                    currentState = State.TURN_LEFT;
                    startwaitTime = -1.0;
                }

                break;
            case TURN_LEFT:
                System.out.println("robot.driveMotors(-0.02, 0.02);");
                robot.driveMotors(-0.02, 0.02);
                if (startTurnTime < 0) {
                    startTurnTime = currentTime;
                }
                System.out.println(currentTime - startTurnTime + " > " + TIME_TO_ROTATE);
                if (currentTime - startTurnTime > TIME_TO_ROTATE) {
                    System.out.println("Changing state to WAIT");
                    // stop rotating
                    currentState = State.WAIT;
                    startTurnTime = 1.0;
                }
                break;
        }

        double time = robot.getTime();
        System.out.println(time + ": Deciding");

        if (robot.isCompassReady())
            System.out.println(time + ": Compass: " + robot.getCompassSensor());

        String[] sensorsToRequest = {"Compass"};
        robot.requestSensors(sensorsToRequest);
    }
}